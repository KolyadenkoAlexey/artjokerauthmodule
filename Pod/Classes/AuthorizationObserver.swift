//
//  AuthorizationObserver.swift
//  Pods
//
//  Created by dev on 20.01.16.
//
//

import Foundation

public protocol AuthorizationObserver {

    // MARK: - Controller Events
    func textFieldsDidValidated(valid: Bool)
    
    // MARK: - Authorization Events
    func authorizationCredentialsReady(login: String, password: String)
    func authorizationCompleted(response: AnyObject?)
    func authorizationFailed(error: NSError?)
    
}