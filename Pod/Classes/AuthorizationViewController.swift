//
//  AuthorizationViewController.swift
//  Career
//
//  Created by dev on 16.01.16.
//  Copyright © 2016 dev. All rights reserved.
//

import UIKit
//import Validator
import RNCryptor

public class AuthorizationViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - Outlets
    @IBOutlet public weak var loginField: BottomBorderTextField!
    @IBOutlet public weak var passwordField: BottomBorderTextField!
    @IBOutlet public weak var loginButton: UIButton!


    
    // MARK: - Nested types
    public struct ErrorMessage {
        public var wrongEmailPatternMessage: String!
        public var wrongPasswordPatternMessage: String!
        public func passwordLengthMessage(min: Int, max: Int) -> String {
            let lengthInfo = "\(min) \(passwordLengthMessageComponents.passwordMessageBetweenLengthInfo) \(max)"
            switch passwordLengthMessageComponents.passwordLengthInfoMessageRelativeLocation as PasswordLengthInfoMessageRelativeLocation {
            case PasswordLengthInfoMessageRelativeLocation.Start:
                return "\(lengthInfo) \(passwordLengthMessageComponents.passwordMessageBeforeLengthInfo) \(passwordLengthMessageComponents.passwordMessageAfterLengthInfo)"
            case PasswordLengthInfoMessageRelativeLocation.End:
                return "\(passwordLengthMessageComponents.passwordMessageBeforeLengthInfo) \(passwordLengthMessageComponents.passwordMessageAfterLengthInfo) \(lengthInfo) "
            case PasswordLengthInfoMessageRelativeLocation.Middle:
                return "\(passwordLengthMessageComponents.passwordMessageBeforeLengthInfo) \(lengthInfo) \(passwordLengthMessageComponents.passwordMessageAfterLengthInfo)"
            }
        }
        
        public var passwordLengthMessageComponents: PasswordLengthMessageComponents
        
        public init(wrongEmailPatternMessage: String!, wrongPasswordPatternMessage: String!, passwordLengthMessageComponents: PasswordLengthMessageComponents) {
            self.wrongEmailPatternMessage = wrongEmailPatternMessage
            self.wrongPasswordPatternMessage = wrongPasswordPatternMessage
            self.passwordLengthMessageComponents = passwordLengthMessageComponents
        }
        

    }
    
    public struct ValidationRule {
        var regexp: String
        var errorMessage: String
        
        public init(regexp: String, errorMessage: String) {
            self.regexp = regexp
            self.errorMessage = errorMessage
        }
        
    }
    
    public struct PasswordLengthMessageComponents {
        
        var passwordMessageBeforeLengthInfo: String!
        var passwordMessageAfterLengthInfo: String!
        var passwordMessageBetweenLengthInfo: String!
        var passwordLengthInfoMessageRelativeLocation: PasswordLengthInfoMessageRelativeLocation
        
        public init(passwordMessageBeforeLengthInfo: String!, passwordMessageAfterLengthInfo: String!, passwordMessageBetweenLengthInfo: String!, passwordLengthInfoMessageRelativeLocation: PasswordLengthInfoMessageRelativeLocation) {            
            self.passwordMessageBeforeLengthInfo = passwordMessageBeforeLengthInfo
            self.passwordMessageAfterLengthInfo = passwordMessageAfterLengthInfo
            self.passwordMessageBetweenLengthInfo = passwordMessageBetweenLengthInfo
            self.passwordLengthInfoMessageRelativeLocation = passwordLengthInfoMessageRelativeLocation
        }
    }
    
    public enum PasswordLengthInfoMessageRelativeLocation {
        case Start
        case Middle
        case End
    }
    
    // MARK: - Public Properties
    public var authURL: (base: String, relative: String)! {
        didSet {
            authExecutor.baseURL = authURL.base
            authExecutor.authorizationRelativePath = authURL.relative
        }
    }
    
    public var authorizationParameters: [String: AnyObject]?
    
    // Endcryption
    var passwordEncryption: HMACAlgo?
    var loginEncryption: HMACAlgo?
    
    // MARK: - Colors
    public var validTextColor: UIColor? = UIColor.blackColor()
    public var errorTextColor: UIColor? = UIColor.redColor()
    
    // Delegate
    public var observer: AuthorizationObserver?
    
    // MARK: - Internal Properties
    var errorMessages: ErrorMessage!
    
    lazy var authExecutor = AuthorizationRequestExecutor()
    
    // MARK: - Lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        setUp(errorMessages: ErrorMessage(
            wrongEmailPatternMessage: "Wrong email pattern", // Wrong email pattern validation message
            wrongPasswordPatternMessage: "Wrong password pattern", // Wrong password pattern validation message
            passwordLengthMessageComponents: PasswordLengthMessageComponents( // Length validation message [Before Info] [Length Info ("6 to 12")] [After Info]
                passwordMessageBeforeLengthInfo: "Password must contains from", // [Before Info]
                passwordMessageAfterLengthInfo: "characters.", // [After Info]
                passwordMessageBetweenLengthInfo: "to", // [Length Info ("6 to 12")]
                passwordLengthInfoMessageRelativeLocation: .End)),
            passwordLength: (min: 6, max: 12))
        
        authURL = ("https://httpbin.org", "/basic-auth/user/password")

    }

    // MARK: - Purpose
    public func logIn() {
        let valid = loginField.validate() && passwordField.validate()
        
        observer?.textFieldsDidValidated(valid)
        
        if valid {
            observer?.authorizationCredentialsReady(loginField.text!, password: passwordField.text!)
            authorizationCredentialsReady(loginField.text!, password: passwordField.text!)
        }
    }
    
    
    // MARK: - Actions
    public func loginButtonPressed(sender: AnyObject) {
        logIn()
    }
    
    // Transformation method
    public func transformCredentialsBeforeRequest(var login: String, var password: String) -> (login: String, password: String) {
        
        if loginEncryption != nil {
            login = HMAC.hash(login, algo: loginEncryption!)
        }
        
        if passwordEncryption != nil {
            password = HMAC.hash(login, algo: passwordEncryption!)
        }
        
        return (login, password)
    }
    
    // MARK: - Authorization Events
    public func authorizationCredentialsReady(login: String, password: String) {
        
        // Transformation
        let credentials = transformCredentialsBeforeRequest(login, password: password)
        authExecutor.authorize(credentials.login, password: credentials.password, parameters: authorizationParameters) { (value, error) -> Void in
            
            if error != nil {
                debugPrint("Error \(error), value \(value)")
                self.authorizationFailed(error)
                self.observer?.authorizationFailed(error)
            }   else {
                debugPrint("Success \(value)")
                self.authorizationCompleted(value)
                self.observer?.authorizationCompleted(value)
            }
        }
    }
    
    public func authorizationCompleted(response: AnyObject?) {
        
    }
    
    public func authorizationFailed(error: NSError?) {
        
    }
    
    // MARK: - Set up
    public func setUp(errorMessages errorMessages: ErrorMessage?, passwordLength: (min: Int, max: Int)?) {
        if errorMessages != nil {
            self.errorMessages = errorMessages!
        }
        
        if self.errorMessages != nil {
            loginField.addRegx(REGEX_EMAIL, withMsg: self.errorMessages.wrongEmailPatternMessage)
        }
        
        if passwordLength != nil {
            setFieldLengthRule(passwordLength!, forField: passwordField, withMessage: self.errorMessages.passwordLengthMessage(passwordLength!.min, max: passwordLength!.max))
        }
        
        configureUI()
    }

    
    public func setUp(loginRules: [ValidationRule], passwordRules: [ValidationRule]) {
        for rule in loginRules {
            configureTextField(loginField, withRule: rule)
        }
        
        for rule in passwordRules {
            configureTextField(passwordField, withRule: rule)
        }
        
        configureUI()
    }
    
    public func setFieldLengthRule(lengthRange: (min: Int, max: Int), forField field: BottomBorderTextField, withMessage message: String) {
        field.addRegx("^\\X{\(lengthRange.min),\(lengthRange.max)}$", withMsg: message)
    }
    
    public func setCredentialEncryption(login login: HMACAlgo?, password: HMACAlgo?) {
        loginEncryption = login
        passwordEncryption = password
    }
    
    public func setErrorIcon(image: UIImage) {
        for field in [loginField, passwordField] {
            field.errorIcon = image
        }
    }
    

    // MARK: - Utility
    func configureTextField(textField: BottomBorderTextField, withRule rule: ValidationRule) {
        textField.addRegx(rule.regexp, withMsg: rule.errorMessage)
    }
    
    public func setUpAuthorizationPath(baseURL: String, relativeURL: String) {
        authURL = (baseURL, relativeURL)
    }
    
    func configureUI() {
        loginField.tag = 1
        passwordField.tag = 2
        
        passwordField.secureTextEntry = true
        loginButton.addTarget(self, action: "loginButtonPressed:", forControlEvents: .TouchUpInside)
        
        loginField.delegate = self
        passwordField.delegate = self
        
        loginField.autocapitalizationType = .None
        loginField.autocorrectionType = .No
        loginField.spellCheckingType = .No
        loginField.returnKeyType = .Next
        
        loginField.validateOnResign = true
        loginField.validateOnCharacterChanged = false
        
        passwordField.validateOnResign = true
        passwordField.validateOnCharacterChanged = false
        
        loginField.presentInView = loginField.superview
        passwordField.presentInView = passwordField.superview
    }
    

    
    // MARK: - UITextFieldDelegate 
    public func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case loginField:
            passwordField.becomeFirstResponder()
        case passwordField:
            logIn()
        default:
            break
        }
        return true
    }
    
    public func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if let validatorField = textField as? BottomBorderTextField {
            validatorField.highlite(false)
            validatorField.validateOnCharacterChanged = false
        }
        
        return true
    }

}
