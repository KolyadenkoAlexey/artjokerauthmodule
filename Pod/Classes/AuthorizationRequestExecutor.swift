//
//  AuthorizationRequestExecutor.swift
//  Career
//
//  Created by dev on 16.01.16.
//  Copyright © 2016 dev. All rights reserved.
//

import UIKit

public class AuthorizationRequestExecutor: NSObject {
    
    var baseURL: String! = "https://httpbin.org/basic-auth/user/password"
    var authorizationRelativePath: String! = ""
    
    func authorize(login: String, password: String, completionHandler: (value: AnyObject?, error: NSError?) -> Void) {
        authorize(login, password: password, parameters: nil, completionHandler: completionHandler)
    }
    
    func authorize(login: String, password: String, parameters: Dictionary<String, AnyObject>?, completionHandler: (value: AnyObject?, error: NSError?) -> Void) {
        
        let userPasswordString = "\(login):\(password)"
        let userPasswordData = userPasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue:0))
        let authString = "Basic \(base64EncodedCredential)"
        
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        config.HTTPAdditionalHeaders = ["Authorization" : authString]
        
        let session = NSURLSession(configuration: config)
        
        let url = NSURL(string: baseURL + authorizationRelativePath)
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        
        if parameters != nil {
            request.HTTPBody = try? NSJSONSerialization.dataWithJSONObject(parameters!, options: NSJSONWritingOptions.init(rawValue: 0))
        }
        
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            if let httpResponse = response as? NSHTTPURLResponse {
                switch httpResponse.statusCode {
                case 200 ... 299:
                    if data != nil {
                        let value = try? NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers)
                        completionHandler(value: value, error: nil)
                    }   else {
                        completionHandler(value: nil, error: nil)
                    }
                default:
                    completionHandler(value: nil, error: NSError(domain: "HTTP_RESPONSE_UNEXPACTABLE_STATUS_CODE", code: 0, userInfo: ["statusCode": httpResponse.statusCode]))
                }
                
                let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                debugPrint(dataString)
                print(dataString)
            }
            
            if error == nil {
                
            }   else {
                completionHandler(value: nil, error: error)
            }
        }
        task.resume()
    }
    
    // Designated initializer
    init(withBaseURL url: String, authorizePath: String) {
        super.init()
        baseURL = url
        authorizationRelativePath = authorizePath
    }
    
    override init() {
        super.init()
    }

}
