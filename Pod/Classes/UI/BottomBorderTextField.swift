//
//  BottomBorderTextField.swift
//  Nobius
//
//  Created by Alexey Kolyadenko on 30.10.15.
//  Copyright © 2015 Alexey Kolyadenko. All rights reserved.
//

import UIKit
//import TextFieldValidator

public class BottomBorderTextField: TextFieldValidator {

    @IBInspectable var currentColor: UIColor!
    @IBInspectable var textFieldStyleColor: UIColor!
    @IBInspectable var textFieldErrorColor: UIColor = UIColor.redColor()
    @IBInspectable var textFieldPlaceholderColor: UIColor!
    var textFieldFont: UIFont = UIFont.systemFontOfSize(14)
    
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override public func drawRect(rect: CGRect) {
        // Drawing code
        addBottomBorderLine(textFieldStyleColor)
    }
    
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        // The resetMe function sets up the values for this button. It is
        // called here when the button first appears and is also called
        // from the main ViewController when all the buttons have been tapped
        // and the app is reset.
        if textFieldStyleColor == nil {
            textFieldStyleColor = UIColor.whiteColor()
        }
        if textFieldPlaceholderColor == nil {
            textFieldPlaceholderColor = textFieldStyleColor.darkerColor()
        }
        currentColor = textFieldStyleColor
        self.turnStyle()
    }
    
    func turnStyle() {
        setFonts(textFieldFont, color: currentColor, placeholderColor: textFieldPlaceholderColor)
        self.borderStyle = UITextBorderStyle.None
        addBottomBorderLine(currentColor)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.turnStyle()
    }
    
    
    
    
    override public func highlite(error: Bool) {
        if (error) {
            currentColor = textFieldErrorColor
        }   else {
            currentColor = textFieldStyleColor
            if !informationalTextField {
                rightView = nil
            }
        }
        setFonts(textFieldFont, color: currentColor, placeholderColor: textFieldPlaceholderColor)
        setNeedsDisplay()
    }

}

// MARK: Essential extensions
extension UITextField {
    
    func addBottomBorderLine(color: UIColor) {
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = color.CGColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func setFonts(font: UIFont, color: UIColor, placeholderColor: UIColor) {
        if attributedPlaceholder != nil {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSForegroundColorAttributeName: placeholderColor, NSFontAttributeName : font])
            
        }
        self.textColor = color
        self.font = font
    }
    
    func setFonts(font: UIFont, color: UIColor) {
        setFonts(font, color: color, placeholderColor: color.darkerColor())
    }
}

extension UIColor {
    func darkerColor() -> UIColor {
        
        var r:CGFloat = 0, g:CGFloat = 0, b:CGFloat = 0, a:CGFloat = 0
        
        if self.getRed(&r, green: &g, blue: &b, alpha: &a){
            return UIColor(red: max(r - 0.2, 0.0), green: max(g - 0.2, 0.0), blue: max(b - 0.2, 0.0), alpha: a)
        }
        
        return UIColor()
    }
    
    func lighterColor() -> UIColor {
        
        var r:CGFloat = 0, g:CGFloat = 0, b:CGFloat = 0, a:CGFloat = 0
        
        if self.getRed(&r, green: &g, blue: &b, alpha: &a){
            return UIColor(red: min(r + 0.2, 1.0), green: min(g + 0.2, 1.0), blue: min(b + 0.2, 1.0), alpha: a)
        }
        
        return UIColor()
    }
}
