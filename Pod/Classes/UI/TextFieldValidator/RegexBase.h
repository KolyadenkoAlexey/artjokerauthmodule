//
//  RegexBase.h
//  FoodDelivery
//
//  Created by Alex Sergienko on 16.01.15.
//  Copyright (c) 2015 Artjoker. All rights reserved.
//
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_ONE_NUMBER_OR_ALPHA_DEFAULT @"[A-Za-z0-9]{1,}"
#define REGEX_USER_NAME_LIMIT @"^.{2,}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{2,}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PHONE_DEFAULT @"[+0-9]{1}[-() 0-9]{6,}"
#define REGEX_ONE_NUMBER_DEFAULT @"[+0-9]{1,}"
#define REGEX_ONE_NUMBER_OR_ALPHA_DEFAULT_RUS @"[A-Za-z0-9А-Яа-я]{1,}"

#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{6,20}"

#define REGEX_DATE @"^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$"

#define REGEX_URL @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
#define REGEX_URL_OPT @"((http|https)://)?((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"

#define REGEX_INSTAGRAM @"/(?:(?:http|https):\/\/)?(?:www.)?(?:instagram.com|instagr.am)\/([A-Za-z0-9-_]+)/igm"
#define REGEX_FACEBOOK @"(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?"
#define REGEX_GOOGLEPLUS @"/([+][\\w-_\\p{L}]+|[\\d]{21})(?:\/)?(?:[\\w-_]+)?$/"