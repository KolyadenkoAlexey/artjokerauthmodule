# ArtJokerAuthModule

[![CI Status](http://img.shields.io/travis/Alexey Kolyadenko/ArtJokerAuthModule.svg?style=flat)](https://travis-ci.org/Alexey Kolyadenko/ArtJokerAuthModule)
[![Version](https://img.shields.io/cocoapods/v/ArtJokerAuthModule.svg?style=flat)](http://cocoapods.org/pods/ArtJokerAuthModule)
[![License](https://img.shields.io/cocoapods/l/ArtJokerAuthModule.svg?style=flat)](http://cocoapods.org/pods/ArtJokerAuthModule)
[![Platform](https://img.shields.io/cocoapods/p/ArtJokerAuthModule.svg?style=flat)](http://cocoapods.org/pods/ArtJokerAuthModule)

## Usage

* Just subclass or create instance of AuthorizationViewController
```swift 
import ArtJokerAuthModule

class MyViewController: AuthorizationViewController {}

```

* Add outlets connection from storyboard to AuthorizationViewController class
```swift 
// MARK: - Outlets
@IBOutlet public weak var loginField: UITextField!
@IBOutlet public weak var passwordField: UITextField!
@IBOutlet public weak var loginButton: UIButton!
```

* Call to set URL
```swift 
setUpAuthorizationPath(baseURL: String, relativeURL: String)
```

* Additional setup with
```swift 
setUp(errorMessages: ErrorMessage(
wrongEmailPatternMessage: "Wrong email pattern", // Wrong email pattern validation message
wrongPasswordPatternMessage: "Wrong password pattern", // Wrong password pattern validation message
passwordLengthMessageComponents: PasswordLengthMessageComponents( // Length validation message [Before Info] [Length Info ("6 to 12")] [After Info]
passwordMessageBeforeLengthInfo: "Password must contains from", // [Before Info]
passwordMessageAfterLengthInfo: "characters.", // [After Info]
passwordMessageBetweenLengthInfo: "to", // [Length Info ("6 to 12")]
passwordLengthInfoMessageRelativeLocation: .End)),
passwordLength: (min: 6, max: 12))
```

* Add parameters to auth request 
```swift 
authorizationParameters = ["first": 132, "sec": "ssss"]
```
* Handle events
```swift 
override func authorizationCompleted(response: AnyObject?) {
debugPrint(response)
/*
handle response
*/
}

override func authorizationFailed(error: NSError?) {
debugPrint(error)
/*
handle error
*/
}
```

* Encrypt credentials
```swift 
setCredentialEncryption(login: .MD5, password: .SHA512)
```

* Observe events with observer
```swift 
// MARK: - Authorization Events
func authorizationCredentialsReady(login: String, password: String)
func authorizationCompleted(response: AnyObject?)
func authorizationFailed(error: NSError?)
```

* Handle controller events with delegate
```swift 
// MARK: - Controller Events
func textFieldsDidValidated(valid: Bool)
```

## Installation

ArtJokerAuthModule is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ArtJokerAuthModule’, :git => 'https://bitbucket.org/KolyadenkoAlexey/artjokerauthmodule.git'
```

## Author

Alexey Kolyadenko, kolyadenko.kks@gmail.com

## License

ArtJokerAuthModule is available under the MIT license. See the LICENSE file for more info.