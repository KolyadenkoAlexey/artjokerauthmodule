//
//  ViewController.swift
//  ArtJokerAuthModule
//
//  Created by Alexey Kolyadenko on 01/22/2016.
//  Copyright (c) 2016 Alexey Kolyadenko. All rights reserved.
//

import UIKit
import ArtJokerAuthModule

class ViewController: AuthorizationViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpAuthorizationPath("https://httpbin.org", relativeURL: "/post")
        setUp(errorMessages: ErrorMessage(
            wrongEmailPatternMessage: "Wrong email pattern", // Wrong email pattern validation message
            wrongPasswordPatternMessage: "Wrong password pattern", // Wrong password pattern validation message
            passwordLengthMessageComponents: PasswordLengthMessageComponents( // Length validation message [Before Info] [Length Info ("6 to 12")] [After Info]
                passwordMessageBeforeLengthInfo: "Password must contains from", // [Before Info]
                passwordMessageAfterLengthInfo: "characters.", // [After Info]
                passwordMessageBetweenLengthInfo: "to", // [Length Info ("6 to 12")]
                passwordLengthInfoMessageRelativeLocation: .End)),
            passwordLength: (min: 6, max: 12))
        
        authorizationParameters = ["first": 132, "sec": "ssss"]
        
        
        setCredentialEncryption(login: .MD5, password: .SHA512)
    }
    
    override func authorizationCompleted(response: AnyObject?) {
        debugPrint(response)
        /*
        handle response
        */
    }
    
    override func authorizationFailed(error: NSError?) {
        debugPrint(error)
        /*
        handle error
        */
    }
    
    
    
    
    
}

